package cmd

import (
	"github.com/spf13/cobra"
	symlinker "gitlab.com/tobiasmeyer/symlinker/pkg"
)

const allowBrokenFlagKey string = "allow-broken"

// replaceCmd represents the replace command
var replaceCmd = &cobra.Command{
	Use:   "replace <old segment> <new segment>",
	Short: "Replaces path segments in symlink targets",
	Args:  cobra.ExactArgs(2),
	RunE: func(cmd *cobra.Command, args []string) error {
		oldSegment := args[0]
		newSegment := args[1]
		allowBroken := mustGetBool(cmd, allowBrokenFlagKey)

		brokenSymlinks, err := symlinker.BrokenSymlinks(".")
		if err != nil {
			return err
		}

		for _, symlink := range brokenSymlinks {
			oldTarget, err := symlink.Target()
			if err != nil {
				return err
			}
			replaced, err := symlink.ReplaceSegment(oldSegment, newSegment)
			if err != nil {
				return err
			}
			if !replaced {
				continue
			}

			newTarget, err := symlink.Target()
			if err != nil {
				return err
			}

			if !allowBroken && symlink.Broken() {
				err = symlink.RewriteTarget(oldTarget)
				if err != nil {
					return err
				}
				cmd.Printf("WARNING %q would have been rewritten from %q to %q, but resulting symlink was broken.\n", string(symlink), oldTarget, newTarget)
				continue
			}

			cmd.Printf("%q: %q -> %q\n", string(symlink), oldTarget, newTarget)
		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(replaceCmd)

	replaceCmd.Flags().Bool(allowBrokenFlagKey, false, "Allow broken rewritten symlinks (Only use when you need multiple replace operations)")
}
