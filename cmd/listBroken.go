package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	symlinker "gitlab.com/tobiasmeyer/symlinker/pkg"
)

// listBrokenCmd represents the listBroken command
var listBrokenCmd = &cobra.Command{
	Use:   "listBroken",
	Short: "Lists broken symlinks",
	Run: func(cmd *cobra.Command, args []string) {
		brokenSymlinks, err := symlinker.BrokenSymlinks(".")
		if err != nil {
			log.Fatalf("%+v\n", err)
		}

		for _, symlink := range brokenSymlinks {
			target, err := symlink.Target()
			if err != nil {
				log.Fatalf("%+v\n", err)
			}
			fmt.Printf("%q -> %q\n", string(symlink), target)
		}
	},
}

func init() {
	rootCmd.AddCommand(listBrokenCmd)
}
