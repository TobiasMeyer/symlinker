# Symlinker
[![pipeline status](https://gitlab.com/TobiasMeyer/symlinker/badges/master/pipeline.svg)](https://gitlab.com/TobiasMeyer/symlinker/-/commits/master)
[![coverage report](https://gitlab.com/TobiasMeyer/symlinker/badges/master/coverage.svg)](https://gitlab.com/TobiasMeyer/symlinker/-/commits/master)

When you use symbolic links or short symlinks and move files or folders the symlink is not automatically updated and therefore becomes broken.
When clicking on a broken symlink most file explorers show a warning with some offering you the option to delete the symlink or manually find the file or folder it should be linking to.
Especially when dealing with a large amount of symlinks or symlinks scattered across very locations this becomes very tideous and at some point not feasible to do by hand.

This CLI tool allows you to **find and fix symbolic links as bulk operations**.
To first get an overview of which symlinks are broken run the `symlinker listBroken` command in the top most folder you want to find links under.
This should allow you to inspect which folders and files have been moved.
If you find for example that the folder at _/totally/boring/path/_ has been moved to _/totally/awesome/path/_ you can run `symlinker replace /totally/boring/path/ /totally/awesome/path/` to rewrite all symlinks targeting the folder or any file/folder inside it to use the new folder.
While it may be tempting to just run `symlinker replace boring awesome` it is recommanded to replace the largest possible matching path segment.
When you run the previous command to replace boring with awesome it can fail to produce the expected results if the path matches the old segment multiple times.

```
Find and fix broken symlinks

Usage:
  symlinker [command]

Available Commands:
  help        Help about any command
  listBroken  Lists broken symlinks
  replace     Replaces path segments in symlink targets

Flags:
  -h, --help   help for symlinker

Use "symlinker [command] --help" for more information about a command.
```
