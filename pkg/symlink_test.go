package symlinker_test

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	symlinker "gitlab.com/tobiasmeyer/symlinker/pkg"
)

func TestBrokenSymlinks(t *testing.T) {
	t.Run("Find nothing inside empty folder", func(t *testing.T) {
		dir, cleanUp := tempDir(t)
		defer cleanUp()

		brokenSymlinks, err := symlinker.BrokenSymlinks(dir)
		if err != nil {
			t.Fatalf("%+v\n", err)
		}
		assert.Empty(t, brokenSymlinks)
	})
	t.Run("Working symlink is ignored", func(t *testing.T) {
		dir, cleanUp := tempDir(t)
		defer cleanUp()
		fName := closedTempFile(t, dir)

		err := os.Symlink(fName, dir+"/test")
		if err != nil {
			t.Fatalf("%+v\n", err)
		}

		brokenSymlinks, err := symlinker.BrokenSymlinks(dir)
		if err != nil {
			t.Fatalf("%+v\n", err)
		}
		assert.Empty(t, brokenSymlinks)
	})
	t.Run("Broken symlink is found", func(t *testing.T) {
		dir, cleanUp := tempDir(t)
		defer cleanUp()
		fName := closedTempFile(t, dir)
		err := os.Symlink(fName, dir+"/test")
		if err != nil {
			t.Fatalf("%+v\n", err)
		}
		os.Remove(fName)

		brokenSymlinks, err := symlinker.BrokenSymlinks(dir)
		if err != nil {
			t.Fatalf("%+v\n", err)
		}
		assert.NotEmpty(t, brokenSymlinks)
	})
}

func TestSymlink(t *testing.T) {
	t.Run("Target", func(t *testing.T) {
		t.Run("Finds target file of valid symlink", func(t *testing.T) {
			dir, cleanUp := tempDir(t)
			defer cleanUp()
			fName := closedTempFile(t, dir)

			err := os.Symlink(fName, dir+"/test")
			if err != nil {
				t.Fatalf("%+v\n", err)
			}

			symlink := symlinker.Symlink(dir + "/test")
			target, err := symlink.Target()
			assert.NoError(t, err)
			assert.NotEmpty(t, target)
		})
		t.Run("Finds target file of broken symlink", func(t *testing.T) {
			dir, cleanUp := tempDir(t)
			defer cleanUp()
			fName := closedTempFile(t, dir)
			err := os.Symlink(fName, dir+"/test")
			if err != nil {
				t.Fatalf("%+v\n", err)
			}
			os.Remove(fName)

			symlink := symlinker.Symlink(dir + "/test")
			target, err := symlink.Target()
			assert.NoError(t, err)
			assert.NotEmpty(t, target)
		})
		t.Run("Finds target directory of valid symlink", func(t *testing.T) {
			dirA, cleanUp := tempDir(t)
			defer cleanUp()
			dirB, cleanUp := tempDir(t)
			defer cleanUp()

			err := os.Symlink(dirA, dirB+"/test")
			if err != nil {
				t.Fatalf("%+v\n", err)
			}

			symlink := symlinker.Symlink(dirB + "/test")
			target, err := symlink.Target()
			assert.NoError(t, err)
			assert.NotEmpty(t, target)
		})
		t.Run("Finds target directory of broken symlink", func(t *testing.T) {
			dirA, cleanUp := tempDir(t)
			cleanUp()
			dirB, cleanUp := tempDir(t)
			defer cleanUp()

			err := os.Symlink(dirA, dirB+"/test")
			if err != nil {
				t.Fatalf("%+v\n", err)
			}

			symlink := symlinker.Symlink(dirB + "/test")
			target, err := symlink.Target()
			assert.NoError(t, err)
			assert.NotEmpty(t, target)
		})
	})
	t.Run("Broken", func(t *testing.T) {
		t.Run("Returns FALSE for valid file symlink", func(t *testing.T) {
			dir, cleanUp := tempDir(t)
			defer cleanUp()
			fName := closedTempFile(t, dir)

			err := os.Symlink(fName, dir+"/test")
			if err != nil {
				t.Fatalf("%+v\n", err)
			}

			symlink := symlinker.Symlink(dir + "/test")
			broken := symlink.Broken()
			assert.False(t, broken)
		})
		t.Run("Returns TRUE for broken file symlink", func(t *testing.T) {
			dir, cleanUp := tempDir(t)
			defer cleanUp()
			fName := closedTempFile(t, dir)
			err := os.Symlink(fName, dir+"/test")
			if err != nil {
				t.Fatalf("%+v\n", err)
			}
			os.Remove(fName)

			symlink := symlinker.Symlink(dir + "/test")
			broken := symlink.Broken()
			assert.True(t, broken)
		})
	})
	t.Run("ReplaceSegment", func(t *testing.T) {
		t.Run("Old segment must not be empty", func(t *testing.T) {
			symlink := symlinker.Symlink("")

			replaced, err := symlink.ReplaceSegment("", "/new/path")

			assert.Error(t, err)
			assert.False(t, replaced)
		})
		t.Run("Both old and new segment must start and end with either slash or non-slash", func(t *testing.T) {
			symlink := symlinker.Symlink("")
			tests := []struct {
				OldSegment, NewSegment string
			}{
				{"old/path", "/new/path"},
				{"/old/path", "new/path"},
				{"/old/path/", "/new/path"},
				{"/old/path/", "new/path"},
				{"old/path", "new/path/"},
			}

			for _, c := range tests {
				replaced, err := symlink.ReplaceSegment(c.OldSegment, c.NewSegment)

				assert.Error(t, err, "%q -> %q", c.OldSegment, c.NewSegment)
				assert.False(t, replaced)
			}

		})
		t.Run("Replaces old with new segment", func(t *testing.T) {
			dirA, cleanUp := tempDir(t)
			defer cleanUp()
			dirB, cleanUp := tempDir(t)
			defer cleanUp()
			fName := closedTempFile(t, dirA)
			assert.NoError(t, os.Symlink(fName, dirA+"/test"))
			symlink := symlinker.Symlink(dirA + "/test")
			oldTarget, err := symlink.Target()
			assert.NoError(t, err)
			assert.Equal(t, fName, oldTarget)

			replaced, err := symlink.ReplaceSegment(dirA, dirB)
			assert.NoError(t, err)
			assert.True(t, replaced)

			newTarget, err := symlink.Target()
			assert.NoError(t, err)
			assert.Contains(t, newTarget, dirB)
		})
		t.Run("Ignores symlink not matching old path segment", func(t *testing.T) {
			dirA, cleanUp := tempDir(t)
			defer cleanUp()
			dirB, cleanUp := tempDir(t)
			defer cleanUp()
			dirC, cleanUp := tempDir(t)
			defer cleanUp()
			fName := closedTempFile(t, dirA)
			assert.NoError(t, os.Symlink(fName, dirA+"/test"))
			symlink := symlinker.Symlink(dirA + "/test")
			oldTarget, err := symlink.Target()
			assert.NoError(t, err)
			assert.Equal(t, fName, oldTarget)

			replaced, err := symlink.ReplaceSegment(dirC, dirB)
			assert.NoError(t, err)
			assert.False(t, replaced)

			newTarget, err := symlink.Target()
			assert.NoError(t, err)
			assert.Equal(t, fName, newTarget)
		})
	})
	t.Run("RewriteTarget", func(t *testing.T) {
		dirA, cleanUp := tempDir(t)
		defer cleanUp()
		dirB, cleanUp := tempDir(t)
		defer cleanUp()
		fName := closedTempFile(t, dirA)
		assert.NoError(t, os.Symlink(fName, dirA+"/test"))
		symlink := symlinker.Symlink(dirA + "/test")
		oldTarget, err := symlink.Target()
		assert.NoError(t, err)
		assert.Equal(t, fName, oldTarget)

		target := dirB + strings.TrimPrefix(oldTarget, dirA)
		err = symlink.RewriteTarget(target)
		assert.NoError(t, err)

		newTarget, err := symlink.Target()
		assert.NoError(t, err)
		assert.Equal(t, target, newTarget)
	})
}

func tempDir(t *testing.T) (string, func()) {
	dirPath, err := ioutil.TempDir("", "symlinker")
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	resolvedDirPath, err := filepath.EvalSymlinks(dirPath)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	return resolvedDirPath, func() {
		os.RemoveAll(resolvedDirPath)
	}
}

func closedTempFile(t *testing.T, dir string) string {
	f, err := ioutil.TempFile(dir, "")
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	err = f.Close()
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	return f.Name()
}
