package symlinker

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
)

// Symlink is a symbolic link to a file or directory
type Symlink string

// Target finds the target file or directory
func (s Symlink) Target() (string, error) {
	return os.Readlink(string(s))
}

// Broken determines if a symlink is broken or not
func (s Symlink) Broken() bool {
	_, err := filepath.EvalSymlinks(string(s))
	return err != nil
}

// ReplaceSegment replaces the old path segment with the new one
func (s Symlink) ReplaceSegment(oldSegment, newSegment string) (bool, error) {
	if oldSegment == "" {
		return false, errors.New("old path segment must not be empty")
	}
	if newSegment != "" && (startsWithSlash(oldSegment) != startsWithSlash(newSegment) || endsWithSlash(oldSegment) != endsWithSlash(newSegment)) {
		return false, errors.New("old path segment and new path segment must both start and end with either slash or non-slash")
	}

	oldTarget, err := s.Target()
	if err != nil {
		return false, err
	}
	newTarget := strings.Replace(oldTarget, oldSegment, newSegment, 1)
	if newTarget == oldTarget {
		return false, nil
	}

	err = s.RewriteTarget(newTarget)
	if err != nil {
		return false, nil
	}

	return true, nil
}

// RewriteTarget rewrites the target
func (s Symlink) RewriteTarget(newTarget string) error {
	err := os.Remove(string(s))
	if err != nil {
		return err
	}
	err = os.Symlink(newTarget, string(s))
	if err != nil {
		return err
	}
	return nil
}

// BrokenSymlinks finds all broken symlinks under the given path
func BrokenSymlinks(rootDirectoryPath string) ([]Symlink, error) {
	brokenSymlinks := make([]Symlink, 0)

	err := filepath.Walk(rootDirectoryPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return errors.Wrapf(err, "accessing path %q failed", path)
		}
		if !isSymbolicLink(info) {
			return nil
		}

		symlink := Symlink(path)
		if symlink.Broken() {
			brokenSymlinks = append(brokenSymlinks, symlink)
		}

		return nil
	})
	if err != nil {
		return nil, errors.Wrapf(err, "walking folder structure at %q failed", rootDirectoryPath)
	}

	return brokenSymlinks, nil
}

func isSymbolicLink(info os.FileInfo) bool {
	return info.Mode()&os.ModeSymlink != 0
}

func startsWithSlash(s string) bool {
	if len(s) == 0 {
		return false
	}

	return string(s[0]) == "/"
}

func endsWithSlash(s string) bool {
	length := len(s)
	if length == 0 {
		return false
	}

	return string(s[length-1]) == "/"
}
